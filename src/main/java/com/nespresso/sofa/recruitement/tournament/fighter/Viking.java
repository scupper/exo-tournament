package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.item.Item.ItemType;

public class Viking extends Fighter<Viking> {

	private static final int VIKING_HEALTH_POINTS = 120;
	
	public Viking() {
		super(VIKING_HEALTH_POINTS);
		equip(ItemType.AXE.toString());
	}

}
