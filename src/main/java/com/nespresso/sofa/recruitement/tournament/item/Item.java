package com.nespresso.sofa.recruitement.tournament.item;

import com.nespresso.sofa.recruitement.tournament.item.exception.NoItemDefinedException;

public abstract class Item {

	protected static final String ARMOR_VALUE = "armor";
	protected static final String SWORD_VALUE = "sword";
	protected static final String AXE_VALUE = "axe";
	protected static final String BUCKLER_VALUE = "buckler";
	protected static final String GREAT_SWORD_VALUE = "greatsword";
	
	protected String name;
	
	protected Item(String name) {
		this.name = name;
	}
	
	public abstract boolean isAttack();
	
	public abstract boolean isDefense();
	
	@Override
	public String toString() {
		return name;
	}

	public static Item instanceForType(ItemType itemType) {
		switch(itemType) {
			case SWORD :
				return new HandSword();
			case ARMOR :
				return new Armor();
			case AXE :
				return new HandAxe();
			case BUCKLER : 
				return new Buckler();
			case GREAT_SWORD :
				return new GreatSword();
			default :
				throw new NoItemDefinedException(itemType.toString());
		}
	}
	
	public static Item createItemForName(String name) {
		ItemType itemType = ItemType.forName(name);
		return instanceForType(itemType);
	}

	public static enum ItemType {
		
		ARMOR(ARMOR_VALUE),
		SWORD(SWORD_VALUE),
		AXE(AXE_VALUE),
		BUCKLER(BUCKLER_VALUE),
		GREAT_SWORD(GREAT_SWORD_VALUE);
		
		private String name;
		
		private ItemType(String name) {
			this.name = name;
		}
		
		public static ItemType forName(String name) {
			if(name == null )
				throw new NoItemDefinedException(name);
			if(name.trim().equalsIgnoreCase(ARMOR_VALUE)) {
				return ARMOR;
			} else if(name.trim().equalsIgnoreCase(SWORD_VALUE)) {
				return SWORD;
			} else if(name.trim().equalsIgnoreCase(AXE_VALUE)) {
				return AXE;
			} else if(name.trim().equalsIgnoreCase(BUCKLER_VALUE)) {
				return BUCKLER;
			} else if(name.trim().equalsIgnoreCase(GREAT_SWORD_VALUE)) {
				return GREAT_SWORD;
			}
			throw new NoItemDefinedException(name);
		}
		
		@Override
		public String toString() {
			return name;
		}
		
	}
	
	
}
