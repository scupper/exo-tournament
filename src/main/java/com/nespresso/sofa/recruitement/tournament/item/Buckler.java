package com.nespresso.sofa.recruitement.tournament.item;

public class Buckler extends DefenseItem {

	private int hitCounter;
	private int axeHitCounter;
	
	protected Buckler() {
		super(Item.BUCKLER_VALUE);
		hitCounter = 0;
		axeHitCounter = 0;
	}
	
	public int calculateDamageAgainst(int expectedDamage, boolean isAxe) {
		if(axeHitCounter > 2) {
			setCanDefend(false);
			return expectedDamage;
		} else {
			if(hitCounter % 2 == 1) {
				setCanDefend(false);
				hitCounter++;
				return expectedDamage;
			} else {
				setCanDefend(true);
				if(isAxe)
					axeHitCounter ++;
				hitCounter++;
				return 0;
			}
		}
	}
	
}
