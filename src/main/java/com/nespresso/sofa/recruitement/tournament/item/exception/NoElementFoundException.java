package com.nespresso.sofa.recruitement.tournament.item.exception;

public class NoElementFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 7939576935934024114L;

	public NoElementFoundException(String e) {
		super("No element found : <"+e+">");
	}
	
}
