package com.nespresso.sofa.recruitement.tournament;

import com.nespresso.sofa.recruitement.tournament.fighter.Fighter;

public class RTSDuel {

	private Fighter<?> fighter1;
	private Fighter<?> fighter2;
	
	public RTSDuel(Fighter<?> fighter1, Fighter<?> fighter2) {
		this.fighter1 = fighter1;
		this.fighter2 = fighter2;
	}
	
	public void launch() {
		while(!Fighter.isAnyFighterDead(fighter1, fighter2)) {
			iterateRounds();
		}
	}
	
	private void iterateRounds() {
		startRound(new Round(fighter1, fighter2));
		startRound(new Round(fighter2, fighter1));
	}
	
	private static void startRound(final Round round) {
		round.start();
	}
	
}
