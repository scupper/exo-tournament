package com.nespresso.sofa.recruitement.tournament.item;

public class DefenseItem extends Item {

	private int damageReductionValue;
	private int attackReductionValue;
	private boolean canDefend;
	
	protected DefenseItem(String name) {
		super(name);
		this.canDefend = true;
		this.attackReductionValue = 0;
		this.damageReductionValue = 0;
	}

	@Override
	public boolean isAttack() {
		return false;
	}

	@Override
	public boolean isDefense() {
		return true;
	}

	public int getDamageReductionValue() {
		return damageReductionValue;
	}

	public void setDamageReductionValue(int damageReductionValue) {
		this.damageReductionValue = damageReductionValue;
	}

	public int getAttackReductionValue() {
		return attackReductionValue;
	}

	public void setAttackReductionValue(int attackReductionValue) {
		this.attackReductionValue = attackReductionValue;
	}

	public boolean isCanDefend() {
		return canDefend;
	}

	public void setCanDefend(boolean canDefend) {
		this.canDefend = canDefend;
	}

}
