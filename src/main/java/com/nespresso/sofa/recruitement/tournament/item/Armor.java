package com.nespresso.sofa.recruitement.tournament.item;

public class Armor extends DefenseItem {

	private static final int RECEIVED_DAMAGES_REDUCTION_VALUE = 3;
	public static final int ATTACK_DAMAGES_REDUCTION_VALUE = 1;
	
	protected Armor() {
		super(Item.ARMOR_VALUE);
		setDamageReductionValue(RECEIVED_DAMAGES_REDUCTION_VALUE);
		setAttackReductionValue(ATTACK_DAMAGES_REDUCTION_VALUE);
	}

}
