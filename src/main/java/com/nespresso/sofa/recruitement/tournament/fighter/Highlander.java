package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.item.Item.ItemType;

public class Highlander extends Fighter<Highlander> {

	private static final int HIGHLANDER_HEALTH_POINTS = 150;
	
	public Highlander() {
		super(HIGHLANDER_HEALTH_POINTS);
		equip(ItemType.GREAT_SWORD.toString());
	}

	public Highlander(String type) {
		this();
		if(type.equalsIgnoreCase("Veteran")) 
			setVeteran(true);
		else 
			throw new UnsupportedOperationException();
	}
}
