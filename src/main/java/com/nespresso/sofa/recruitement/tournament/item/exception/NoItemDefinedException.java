package com.nespresso.sofa.recruitement.tournament.item.exception;

public class NoItemDefinedException extends RuntimeException {

	private static final long serialVersionUID = -755821826588813028L;

	public NoItemDefinedException(String itemName) {
		super("No Item is defined for : <"+itemName+">");
	}
	
}
