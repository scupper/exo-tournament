package com.nespresso.sofa.recruitement.tournament.item.exception;

public class WrongEquipementException extends RuntimeException {

	private static final long serialVersionUID = -5048345333292109159L;

	public WrongEquipementException(String equip) {
		super("Wrong Equipment : <"+equip+">");
	}
	
}
