package com.nespresso.sofa.recruitement.tournament.item;

public class AttackItem extends Item {

	private int offsetAttackHitPoint;
	private boolean canAttack;
	private int damage;
	private boolean poisonous;
	private int poisonUseCount;
	private boolean goneBerzerk;
	
	private final static int POINT_HIT_POINT_VALUE = 20;
	
	protected AttackItem(String name, int damage) {
		super(name);
		this.damage = damage;
		this.poisonUseCount = 0;
		this.canAttack = true;
		this.offsetAttackHitPoint = 0 ;
	}

	@Override
	public boolean isAttack() {
		return true;
	}

	@Override
	public boolean isDefense() {
		return false;
	}

	public int getOffsetAttackHitPoint() {
		return offsetAttackHitPoint;
	}

	public void setOffsetAttackHitPoint(int offsetAttackHitPoint) {
		this.offsetAttackHitPoint = offsetAttackHitPoint;
	}

	public boolean isCanAttack() {
		return canAttack;
	}

	public void setCanAttack(boolean canAttack) {
		this.canAttack = canAttack;
	}

	public int getDamage() {
		int offset = offsetAttackHitPoint;
		if(isCanAttack()) {
			if(isPoisonous()) {
				offset+=POINT_HIT_POINT_VALUE;
				poisonUseCount++;
				if(poisonUseCount > 1) {
					setPoisonous(false);
				}
			}
			int d = damage + offset;
			if(isGoneBerzerk()) {
				d+=d;
			}
			return d;
		}
		return 0;
	}

	public boolean isPoisonous() {
		return this.poisonous;
	}

	public void setPoisonous(boolean poisonous) {
		this.poisonous = poisonous;
	}

	public boolean isGoneBerzerk() {
		return goneBerzerk;
	}

	public void setGoneBerzerk(boolean goneBerzerk) {
		this.goneBerzerk = goneBerzerk;
	}

}
