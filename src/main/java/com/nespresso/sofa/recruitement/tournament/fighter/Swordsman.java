package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.item.Item.ItemType;

public class Swordsman extends Fighter<Swordsman> {

	private static final int SWORDSMAN_HEALTH_POINTS = 100;
	
	public Swordsman() {
		super(SWORDSMAN_HEALTH_POINTS);
		equip(ItemType.SWORD.toString());
	}
	
	public Swordsman(String type) {
		super(SWORDSMAN_HEALTH_POINTS);
		if(type.equalsIgnoreCase("Vicious")) 
			setUsePoison(true);
		else 
			throw new UnsupportedOperationException();
	}

}
