package com.nespresso.sofa.recruitement.tournament.fighter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.nespresso.sofa.recruitement.tournament.RTSDuel;
import com.nespresso.sofa.recruitement.tournament.item.Armor;
import com.nespresso.sofa.recruitement.tournament.item.AttackItem;
import com.nespresso.sofa.recruitement.tournament.item.Buckler;
import com.nespresso.sofa.recruitement.tournament.item.DefenseItem;
import com.nespresso.sofa.recruitement.tournament.item.HandAxe;
import com.nespresso.sofa.recruitement.tournament.item.Item;
import com.nespresso.sofa.recruitement.tournament.item.exception.NoElementFoundException;
import com.nespresso.sofa.recruitement.tournament.item.exception.WrongEquipementException;

public class Fighter<T extends Fighter<T>> {

	private static final float EPSILON = 0.000000001f;
	
	protected List<Item> ownedItems;
	private int healthPoints;
	private int hitPoints;
	private boolean usePoison;
	protected boolean veteran;

	public Fighter(int healthPoints) {
		this.healthPoints = healthPoints;
		this.hitPoints = healthPoints;
		this.ownedItems = new ArrayList<>();
		this.usePoison = false;
		this.veteran = false;
	}
	
	public Fighter(int healthPoints, boolean usePoison, boolean veteran) {
		this.healthPoints = healthPoints;
		this.hitPoints = healthPoints;
		this.ownedItems = new ArrayList<>();
		this.usePoison = usePoison;
		this.veteran = veteran;
	}

	@SuppressWarnings("unchecked")
	public T equip(String name) {
		handleEquipForName(name);
		Item item = Item.createItemForName(name);
		ownedItems.add(item);
		return (T) this;
	}

	public void engage(Fighter<?> fighter) {
		prepareItemsForEngage();
		RTSDuel duel = new RTSDuel(this, fighter);
		duel.launch();
	}
	
	private void prepareItemsForEngage() {
		poisonAttackItems();
	}
	
	private void handleEquipForName(String name) {
		disallowDuplicateEquipments(name);
	}
	
	private void poisonAttackItems() {
		if(ownAttackItems()) {
			getAttackItems()
				.forEach(attackItem -> {
					usePoisonOnAttackItem(attackItem, usePoison);
				});
		}
	}
	
	private static void usePoisonOnAttackItem(AttackItem item, boolean usePoison) {
		item.setPoisonous(usePoison);
	}

	private void disallowDuplicateEquipments(String name) {
		if (this.ownedItems.stream().anyMatch(ownedItem -> ownedItem.toString().equalsIgnoreCase(name)))
			throw new WrongEquipementException(name);
	}

	public void inflictDamage(int damage) {
		hitPoints -= damage;
		if (hitPoints < 0)
			hitPoints = 0;
	}

	public int hitPoints() {
		return hitPoints;
	}

	public float getHealthLeftInPercentage() {
		return (hitPoints * 100.0f) / healthPoints;
	}
	
	public boolean isBerzerk() {
		if(veteran)
			return(getHealthLeftInPercentage() - 30.0f < EPSILON);
		return false;
	}

	public boolean isDead() {
		return hitPoints <= 0 ? true : false;
	}

	public void setUsePoison(boolean usePoison) {
		this.usePoison = usePoison;
	}
	
	public void setVeteran(boolean veteran) {
		this.veteran = veteran;
	}
	
	public boolean ownArmor() {
		return ownedItems.stream().anyMatch(item -> item instanceof Armor);
	}

	public Armor getArmor() {
		if (!ownArmor()) {
			throw new NoElementFoundException("Armor");
		}
		return ownedItems.stream().filter(item -> item instanceof Armor).map(armor -> (Armor) armor).findFirst()
				.orElse(null);
	}

	public boolean ownNonArmorDefenseItems() {
		return ownedItems.stream().anyMatch(item -> item.isDefense() && !(item instanceof Armor));
	}

	public List<DefenseItem> getNonArmorDefenseItems() {
		if (!ownNonArmorDefenseItems()) {
			throw new NoElementFoundException("Non Armor");
		}
		return ownedItems.stream().filter(item -> item.isDefense() && !(item instanceof Armor))
				.map(nonArmor -> (DefenseItem) nonArmor).collect(Collectors.toList());
	}

	public static boolean isAnyFighterDead(Fighter<?> fighter1, Fighter<?> fighter2) {
		return fighter1.isDead() || fighter2.isDead();
	}
	
	public boolean ownBuckler() {
		return ownedItems.stream().anyMatch(item -> item instanceof Buckler);
	}

	public Buckler getBuckler() {
		if (!ownBuckler()) {
			throw new NoElementFoundException("Buckler");
		}
		return ownedItems
				.stream()
				.filter(item -> item instanceof Buckler)
				.map(armor -> (Buckler) armor)
				.findFirst()
					.orElse(null);
	}

	public boolean ownHandAxe() {
		return ownedItems.stream().anyMatch(item -> item instanceof HandAxe);
	}

	public List<HandAxe> getHandAxes() {
		if (!ownHandAxe()) {
			throw new NoElementFoundException("HandAxe");
		}
		return ownedItems
					.stream()
					.filter(item -> item instanceof HandAxe)
					.map(axe -> (HandAxe) axe)
					.collect(Collectors.toList());
	}
	
	public boolean ownAttackItems() {
		return ownedItems
					.stream()
					.anyMatch(item -> item instanceof AttackItem);
	}

	public List<AttackItem> getAttackItems() {
		if (!ownAttackItems()) {
			throw new NoElementFoundException("Attack Items");
		}
		return ownedItems
					.stream()
					.filter(item -> item instanceof AttackItem)
					.map(attackItem -> (AttackItem) attackItem)
					.collect(Collectors.toList());
	}
	
}
