package com.nespresso.sofa.recruitement.tournament.item;

public class HandAxe extends AttackItem {

	private static final int DAMAGE = 6;
	
	protected HandAxe() {
		super(Item.AXE_VALUE, DAMAGE);
	}

}
