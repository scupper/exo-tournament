package com.nespresso.sofa.recruitement.tournament.item;

public class HandSword extends AttackItem {

	private static int DAMAGE = 5;
	
	protected HandSword() {
		super(Item.SWORD_VALUE, DAMAGE);
	}

}
