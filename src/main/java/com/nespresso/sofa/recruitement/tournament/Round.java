package com.nespresso.sofa.recruitement.tournament;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.nespresso.sofa.recruitement.tournament.fighter.Fighter;
import com.nespresso.sofa.recruitement.tournament.item.AttackItem;
import com.nespresso.sofa.recruitement.tournament.item.Buckler;
import com.nespresso.sofa.recruitement.tournament.item.HandAxe;
import com.nespresso.sofa.recruitement.tournament.item.Item;

public class Round {

	private Fighter<?> attacker;
	private Fighter<?> attackee;

	public Round(Fighter<?> attacker, Fighter<?> attackee) {
		this.attacker = attacker;
		this.attackee = attackee;
	}

	public void start() {
		launch();
	}

	private void launch() {
		if (!Fighter.isAnyFighterDead(attacker, attackee)) {
			attackee.inflictDamage(calculateDamage());
		}
	}

	private int calculateDamage() {
		int reduceDamageValue = 0;
		int reduceAttackValue = 0;
		if (attackee.ownArmor())
			reduceDamageValue = attackee.getArmor().getDamageReductionValue();
		if (attacker.ownArmor())
			reduceAttackValue = attacker.getArmor().getAttackReductionValue();
		List<AttackItem> attackItems = attacker.getAttackItems();
		if (attacker.isBerzerk())
			doubleDamage(attackItems);
		int calculatedDamage = 0;
		if (attackee.ownBuckler())
			calculatedDamage = calculateDamageWithBuckler(attackItems, reduceAttackValue, reduceDamageValue);
		else
			calculatedDamage = calculateDamageWithoutBuckler(attackItems, reduceAttackValue, reduceDamageValue);
		return calculatedDamage;
	}

	private int calculateDamageWithBuckler(List<AttackItem> attackItems, int finalReduceAttackValue,
			int reduceDamageValue) {
		Buckler buckler = attackee.getBuckler();
		AtomicInteger calculatedDamageFromBuckler = new AtomicInteger(0);
		attackItems.forEach(attackItem -> {
			if (attackItem.isCanAttack())
				calculatedDamageFromBuckler.addAndGet(buckler.calculateDamageAgainst(
						attackItem.getDamage() - finalReduceAttackValue, isHandAxe(attackItem)));
			else
				attackItem.getDamage();
		});
		int finalDamage = calculatedDamageFromBuckler.get() - reduceDamageValue;
		return finalDamage <= 0 ? 0 : finalDamage;
	}

	private int calculateDamageWithoutBuckler(List<AttackItem> attackItems, int finalReduceAttackValue,
			int reduceDamageValue) {
		AtomicInteger calculatedDamage = new AtomicInteger(0);
		attackItems.forEach(attackItem -> {
			calculatedDamage.addAndGet(attackItem.getDamage() - finalReduceAttackValue);
		});
		int finalDamage = calculatedDamage.get() - reduceDamageValue;
		return finalDamage <= 0 ? 0 : finalDamage;
	}

	private static void doubleDamage(List<AttackItem> attackItems) {
		attackItems.forEach(attackItem -> {
			attackItem.setGoneBerzerk(true);
		});
	}

	private static boolean isHandAxe(Item item) {
		return item instanceof HandAxe;
	}

}
