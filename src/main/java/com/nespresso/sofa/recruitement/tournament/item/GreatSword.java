package com.nespresso.sofa.recruitement.tournament.item;

public class GreatSword extends AttackItem {

	private static final int DAMAGE = 12;
	
	private int hitCount;
	
	protected GreatSword() {
		super(Item.GREAT_SWORD_VALUE, DAMAGE);
		hitCount = 0;
	}

	public int getDamage() {
		if(hitCount%3 == 2) {
			setCanAttack(false);
		} else {
			setCanAttack(true);
		}
		hitCount++;
		return super.getDamage();
	}
	
}
